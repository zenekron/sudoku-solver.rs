#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Cell {
    Empty { candidates: [bool; 9], len: usize },
    Full(u8),
}

impl Default for Cell {
    fn default() -> Self {
        Cell::Empty {
            candidates: [true; 9],
            len: 9,
        }
    }
}

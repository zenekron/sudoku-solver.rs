extern crate rayon;

mod board;
mod cell;
mod coord;
mod errors;

use board::Board;
use std::env;
use std::fs;

fn main() -> Result<(), anyhow::Error> {
    let args = env::args().collect::<Vec<String>>();

    for file in args.iter().skip(1) {
        println!("processing file '{}'\n", file);

        let file_contents = fs::read_to_string(file)?;
        let board = file_contents.parse::<Board>()?;

        println!("{}\n", board);

        let solutions = board.find_solutions()?;

        println!("{} solutions found\n", solutions.len());
    }

    Ok(())
}

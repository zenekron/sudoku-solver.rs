use crate::coord::Coord;
use thiserror::Error;

#[derive(Debug, Error)]
#[error("invalid character '{character}' at {coord:?}")]
pub struct InvalidCharacterError {
    pub coord: Coord,
    pub character: char,
}

#[derive(Debug, Error)]
pub enum UpdateBoardError {
    #[error("{candidate} is not a valid candidate for cell {coord:?}")]
    InvalidCandidate { coord: Coord, candidate: u8 },
    #[error(
        "cannot write {new_value} to cell {coord:?} because it already has value {actual_value}"
    )]
    CellNotEmpty {
        coord: Coord,
        new_value: u8,
        actual_value: u8,
    },
}

#[derive(Debug, Error)]
pub enum BoardError {
    #[error("performed an illegal operation")]
    IllegalOperation(#[from] UpdateBoardError),
}

#[derive(Debug, Error)]
#[error(
    "cannot remove candidate {candidate} from cell {coord:?} because it already has value {value}"
)]
pub struct UpdateFullCellError {
    pub coord: Coord,
    pub candidate: u8,
    pub value: u8,
}

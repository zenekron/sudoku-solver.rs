#[derive(Debug, Clone, Copy)]
pub struct Coord {
    pub row: usize,
    pub col: usize,
}

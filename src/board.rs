use crate::cell::Cell;
use crate::coord::Coord;
use crate::errors::BoardError;
use crate::errors::InvalidCharacterError;
use crate::errors::UpdateBoardError;
use core::fmt::Display;
use core::ops::Index;
use core::ops::IndexMut;
use core::str::FromStr;
use rayon::prelude::*;
use std::cmp::Ordering;

#[derive(Clone, Copy)]
pub struct Board {
    cells: [Cell; 9 * 9],
}

impl Board {
    fn filter_cells(&self, mut coords: Vec<Coord>) -> Vec<Coord> {
        coords.sort_by(|a: &Coord, b: &Coord| match (self[a], self[b]) {
            (Cell::Empty { len: a_len, .. }, Cell::Empty { len: b_len, .. }) => b_len.cmp(&a_len),
            (Cell::Empty { .. }, Cell::Full(_)) => Ordering::Less,
            (Cell::Full(_), Cell::Empty { .. }) => Ordering::Greater,
            (Cell::Full(_), Cell::Full(_)) => Ordering::Equal,
        });

        let trunc_at = coords
            .iter()
            .enumerate()
            .find(|(_, c)| match self[c] {
                Cell::Empty { .. } => false,
                Cell::Full(_) => true,
            })
            .map_or(coords.len(), |(i, _)| i);

        coords.truncate(trunc_at);
        coords
    }

    pub fn find_solutions(mut self) -> Result<Vec<Board>, BoardError> {
        let coords: Vec<Coord> = (1..=9)
            .flat_map(|row| (1..=9).map(move |col| Coord { row, col }))
            .collect();

        let mut coords = self.filter_cells(coords);

        while let Some(coord) = coords.pop() {
            match self[&coord] {
                Cell::Empty { candidates, len } => {
                    match len {
                        0 => {
                            return Ok(Vec::new());
                        }
                        1 => {
                            let mut value = 0;

                            for (i, candidate) in candidates.iter().enumerate() {
                                if *candidate == true {
                                    value = i + 1;
                                }
                            }

                            self.set(&coord, value as u8)?;
                            coords = self.filter_cells(coords);
                        }
                        _ => {
                            let results = candidates
                                .par_iter()
                                .enumerate()
                                .filter(|(_, b)| **b == true)
                                .map(|(i, _)| i as u8 + 1)
                                .map(|value| {
                                    let mut new_board = self.clone();
                                    new_board.set(&coord, value)?;
                                    new_board.find_solutions()
                                })
                                .collect::<Result<Vec<_>, _>>();

                            return match results {
                                Ok(mat) => Ok(mat.into_iter().flatten().collect()),
                                Err(e) => Err(e),
                            };
                        }
                    };
                }
                _ => unreachable!(),
            }
        }

        Ok(vec![self])
    }

    fn get_related_coords(coord: &Coord) -> impl Iterator<Item = Coord> {
        let Coord { row, col } = *coord;

        let get_starting_pos = |n: usize| ((n - 1) / 3) * 3 + 1;
        let start_row = get_starting_pos(row);
        let start_col = get_starting_pos(col);

        let sec_iter = (start_row..(start_row + 3))
            .flat_map(move |row| (start_col..(start_col + 3)).map(move |col| Coord { row, col }));

        let col_iter = (1..=9).map(move |row| Coord { row, col });
        let row_iter = (1..=9).map(move |col| Coord { row, col });

        row_iter.chain(col_iter).chain(sec_iter)
    }

    fn remove_candidate(&mut self, coord: &Coord, value: u8) {
        if let Cell::Empty { candidates, len } = &mut self[coord] {
            if candidates[value as usize - 1] == true {
                candidates[value as usize - 1] = false;
                *len -= 1;
            }
        }
    }

    pub fn set(&mut self, coord: &Coord, value: u8) -> Result<(), UpdateBoardError> {
        match self[coord] {
            Cell::Empty { candidates, .. } => {
                if candidates[value as usize - 1] == true {
                    self[coord] = Cell::Full(value);
                    Ok(())
                } else {
                    Err(UpdateBoardError::InvalidCandidate {
                        coord: *coord,
                        candidate: value,
                    })
                }
            }
            Cell::Full(v) => Err(UpdateBoardError::CellNotEmpty {
                coord: *coord,
                new_value: value,
                actual_value: v,
            }),
        }?;

        Board::get_related_coords(coord).for_each(|coord| {
            self.remove_candidate(&coord, value);
        });

        Ok(())
    }
}

impl Default for Board {
    fn default() -> Self {
        Board {
            cells: [Cell::default(); 9 * 9],
        }
    }
}

impl Display for Board {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "Board {{")?;
        writeln!(f, "    +-------+-------+-------+")?;

        for row in 1..=9 {
            write!(f, "    | ")?;

            for col in 1..=9 {
                write!(
                    f,
                    "{} ",
                    match self[&Coord { row, col }] {
                        Cell::Empty {
                            candidates: _,
                            len: _,
                        } => "?".to_string(),
                        Cell::Full(v) => v.to_string(),
                    }
                )?;

                if col % 3 == 0 {
                    write!(f, "| ")?;
                }
            }

            write!(f, "\n")?;
            if row % 3 == 0 {
                writeln!(f, "    +-------+-------+-------+")?;
            }
        }

        write!(f, "}}")
    }
}

impl FromStr for Board {
    type Err = InvalidCharacterError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut board = Board::default();

        let line_iter = s
            .trim()
            .lines()
            .take(9)
            .into_iter()
            .enumerate()
            .map(|(row, line)| (row + 1, line));

        for (row, line) in line_iter {
            let char_iter = line
                .chars()
                .take(9)
                .enumerate()
                .map(|(col, c)| (col + 1, c));

            for (col, character) in char_iter {
                if character != '.' {
                    let value: u8 = match character.to_string().parse() {
                        Ok(v) => v,
                        Err(_) => {
                            return Err(InvalidCharacterError {
                                coord: Coord { row, col },
                                character,
                            });
                        }
                    };

                    board.set(&Coord { row, col }, value).unwrap();
                }
            }
        }

        Ok(board)
    }
}

impl Index<&Coord> for Board {
    type Output = Cell;

    fn index(&self, index: &Coord) -> &Self::Output {
        let Coord { row, col } = index;

        &self.cells[(row - 1) * 9 + col - 1]
    }
}

impl IndexMut<&Coord> for Board {
    fn index_mut(&mut self, index: &Coord) -> &mut Self::Output {
        let Coord { row, col } = index;

        &mut self.cells[(row - 1) * 9 + col - 1]
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn default_board() {
        let board = Board::default();

        for cell in board.cells.iter() {
            assert_eq!(cell, &Cell::default());
        }
    }

    #[test]
    fn set_updates_value() {
        let mut board = Board::default();
        let coord = Coord { row: 4, col: 4 };

        assert_eq!(board[&coord], Cell::default());

        board.set(&coord, 9).unwrap();

        assert_eq!(board[&coord], Cell::Full(9));
    }

    #[test]
    fn set_updates_row_candidates() {
        let mut board = Board::default();
        let coord = Coord { row: 4, col: 4 };

        board.set(&coord, 9).unwrap();

        let expected_cell = Cell::Empty {
            candidates: [true, true, true, true, true, true, true, true, false],
            len: 8,
        };

        for row in (1..=9).filter(|x| *x != coord.row) {
            assert_eq!(
                board[&Coord {
                    row,
                    col: coord.col
                }],
                expected_cell
            );
        }
    }

    #[test]
    fn set_updates_column_candidates() {
        let mut board = Board::default();
        let coord = Coord { row: 4, col: 4 };

        board.set(&coord, 9).unwrap();

        let expected_cell = Cell::Empty {
            candidates: [true, true, true, true, true, true, true, true, false],
            len: 8,
        };

        for col in (1..=9).filter(|x| *x != coord.row) {
            assert_eq!(
                board[&Coord {
                    row: coord.row,
                    col
                }],
                expected_cell
            );
        }
    }

    #[test]
    fn set_updates_sector_candidates() {
        let mut board = Board::default();
        let coord = Coord { row: 4, col: 4 };

        board.set(&coord, 9).unwrap();

        let expected_cell = Cell::Empty {
            candidates: [true, true, true, true, true, true, true, true, false],
            len: 8,
        };

        for i in 4..=6 {
            if i == coord.row && i == coord.col {
                continue;
            }

            assert_eq!(board[&Coord { row: i, col: i }], expected_cell);
        }
    }
}

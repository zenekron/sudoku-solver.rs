const assert = require('assert');

function unpack(contents) {
	const packageIndex = contents.indexOf('[package]');
	assert(packageIndex >= 0);

	const versionIndex = contents.indexOf('version', packageIndex);
	assert(versionIndex > packageIndex);

	const startQuoteIndex = contents.indexOf('"', versionIndex);
	assert(startQuoteIndex > versionIndex);

	const endQuoteIndex = contents.indexOf('"', startQuoteIndex + 1);
	assert(endQuoteIndex > startQuoteIndex);

	return [
		contents.slice(0, startQuoteIndex + 1),
		contents.slice(startQuoteIndex + 1, endQuoteIndex),
		contents.slice(endQuoteIndex),
	];
}

function readVersion(contents) {
	const [, version, ] = unpack(contents);

	return version;
}

function writeVersion(contents, version) {
	const [before, , after] = unpack(contents);

	return before + version + after;
}

module.exports = {
	readVersion,
	writeVersion,
};
